import config from "../config";
import {getToken} from "../common/auth";

const API_BASE_URL = config.api.baseUrl;


export const bodyOf = async (requestPromise) => {
  try {
    const response = await requestPromise;
    return response.body;
  } catch (e) {
    throw e;
  }
};

export const getRequestHeaders = (body, token) => {
  const headers = body
    ? {'Accept': 'application/json', 'Content-Type': 'application/json'}
    : {'Accept': 'application/json'};

  if (token) {
    return {...headers, Authorization: `Bearer ${token}`};
  }

  return headers;
};

const handleResponse = async (path, response) => {
  try {
    const status = response.status;

    if (status >= 400) {
      const error = await response.text();
      if (error) {
        throw new Error(error);
      }
    }

    const responseBody = await response.text();
    return {
      status: response.status,
      headers: response.headers,
      body: responseBody ? JSON.parse(responseBody) : null
    };
  } catch (e) {
    throw e;
  }
};

const request = async (method, path, body, auth) => {
  try {
    const response = await sendRequest(method, path, body, auth);
    return handleResponse(
      path,
      response
    );
  }
  catch (error) {
    throw error;
  }
};

export const url = (path, auth) => {
  const apiRoot = API_BASE_URL;
  return path.indexOf('/') === 0
    ? apiRoot + path
    : apiRoot + '/' + path;
};

export const sendRequest = async (method, path, body, auth) => {
  try {
    const endpoint = url(path, auth);
    const token = await getToken();
    const headers = getRequestHeaders(body, token);
    const options = body
      ? {method, headers, body: JSON.stringify(body)}
      : {method, headers};
    return fetch(endpoint, options);
  } catch (e) {
    throw new Error(e);
  }
};

//Public methods
export const get = async (path) => bodyOf(request('get', path, null));

export const post = async (path, body, auth) => bodyOf(request('post', path, body, auth));

export const put = async (path, body) => bodyOf(request('put', path, body));

export const del = async (path) => bodyOf(request('delete', path, null));
