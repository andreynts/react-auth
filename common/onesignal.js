import { AsyncStorage } from "react-native";
import OneSignal from "react-native-onesignal";

const ONESIGNAL_USERID = "APP:ONESIGNAL_USERID";
const ONESIGNAL_PUSHTOKEN = "APP:ONESIGNAL_PUSHTOKEN";

export const getDeviceUserId = () => AsyncStorage.getItem(ONESIGNAL_USERID);
export const getDevicePushToken = () => AsyncStorage.getItem(ONESIGNAL_PUSHTOKEN);

export const sendOneSignalTags = (user) => {
    OneSignal.sendTags({"email" : user.email, "id" : user.id});
}
