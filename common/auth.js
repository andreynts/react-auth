import { AsyncStorage } from "react-native";
const APP_PREFIX = "APP_TOKEN";

export const getToken = () => AsyncStorage.getItem(APP_PREFIX);

export const setToken = token => AsyncStorage.setItem(APP_PREFIX, token);

export const removeToken = () => AsyncStorage.removeItem(APP_PREFIX);
