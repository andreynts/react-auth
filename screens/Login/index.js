// @flow
import React, { PureComponent } from "react";
import type { Node } from "react";
import { Image } from "react-native";
import { Container, Content, Body, Button, Text } from "native-base";
import styles from "./styles";
const logo = require("../../../assets/images/logo.png");

interface Props {
  onLogin: Function;
  onForgot: Function;
  error: string;
  children?: Node;
  isValid: boolean;
}

class Login extends PureComponent<Props> {
  btn = (isValid: boolean) => ({
    marginTop: 30,
    alignSelf: "center",
    backgroundColor: "transparent",
    width: "100%",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: isValid ? "#00D1B9" : "#00D1B94D",
    borderRadius: 10
  });

  btnText = (isValid: boolean) => ({
    color: isValid ? "#00D1B9" : "#00D1B94D"
  });

  render() {
    const { error, onLogin, onForgot, isValid } = this.props;
    return (
      <Container>
        <Content style={styles.container}>
          <Body style={styles.logo}>
            <Image source={logo} resizeMode="contain" />
          </Body>
          <Text style={styles.error}>{error}</Text>
          {this.props.children}
          <Button
            style={this.btn(isValid)}
            onPress={() => isValid && onLogin()}
          >
            <Text style={this.btnText(isValid)}>Login</Text>
          </Button>
          <Text style={styles.forgot} onPress={() => onForgot()}>
            Forgot password?
          </Text>
        </Content>
      </Container>
    );
  }
}

export default Login;
