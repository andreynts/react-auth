import { StyleSheet } from "react-native";

const styles: any = StyleSheet.create({
  container: {
    backgroundColor: "#272727",
    padding: 50
  },
  logo: {
    alignItems: "center",
    justifyContent: "center",
    height: 100
  },
  btn: {
    marginTop: 30,
    alignSelf: "center",
    backgroundColor: "transparent",
    width: "100%",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "#00D1B9",
    borderRadius: 10
  },
  btnText: {
    color: "#00D1B9"
  },
  error: {
    color: "red",
    textAlign: "center"
  },
  forgot: {
    fontSize: 12,
    paddingTop: 100,
    textAlign: "center"
  }
});
export default styles;
