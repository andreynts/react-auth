const styles = {
  item: {
    marginLeft: 0,
    marginRight: 0
  },
  label: {
    color: "#95989A",
    fontSize: 14
  },
  input: { color: "white" },
  toast: { textAlign: "center" }
};

export default styles;
