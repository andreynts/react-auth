// @flow
import * as React from "react";
import { connect } from "react-redux";
import { Item, Input, Toast, Form, Label } from "native-base";
import { Field, reduxForm, formValueSelector } from "redux-form";
import Login from "../../stories/screens/Login";
import { login } from "../../actions/auth";
import styles from "./styles";

import * as validations from "../../validations";

export interface Props {
  navigation: any;
  valid: boolean;
  email: string;
  password: string;
  login: Function;
  error: string;
}

class LoginContainer extends React.Component<Props> {
  renderInput({ input, label, type, meta: { touched, error, warning } }) {
    return (
      <Item floatingLabel error={error && touched} style={styles.item}>
        <Label style={styles.label}>
          {input.name === "email" ? "Email" : "Password"}
        </Label>
        <Input
          autoFocus={input.name === "email"}
          style={styles.input}
          secureTextEntry={input.name === "password" ? true : false}
          keyboardType={input.name === "email" ? "email-address" : "default"}
          autoCapitalize="none"
          {...input}
        />
      </Item>
    );
  }

  handleLogin = () => {
    if (this.props.valid) {
      this.props.login(
        this.props.email,
        this.props.password,
        this.props.navigation.navigate
      );
    } else {
      Toast.show({
        text: "Enter Valid Username & password!",
        duration: 2000,
        position: "top",
        textStyle: styles.toast
      });
    }
  };

  handleForgot = () => this.props.navigation.navigate("ResetPasswordContainer");

  render() {
    return (
      <Login
        onLogin={() => this.handleLogin()}
        onForgot={() => this.handleForgot()}
        error={this.props.error}
        isValid={this.props.valid}
      >
        <Form>
          <Field
            name="email"
            component={this.renderInput}
            validate={[validations.email, validations.required]}
          />
          <Field
            name="password"
            component={this.renderInput}
            validate={[
              validations.alphaNumeric,
              validations.minLength(8),
              validations.maxLength(15),
              validations.required
            ]}
          />
        </Form>
      </Login>
    );
  }
}

const selector = formValueSelector("login");

const mapState = state => ({
  error: state.auth.error,
  email: selector(state, "email"),
  password: selector(state, "password")
});

const mapDispatch = {
  login
};

LoginContainer = connect(mapState, mapDispatch)(LoginContainer);

export default reduxForm({
  form: "login"
})(LoginContainer);
