import { setToken, removeToken } from "../common/auth";
import { getDeviceUserId, getDevicePushToken, sendOneSignalTags } from "../common/onesignal";

import {post, get} from "../util/api";
import { createDevice } from "./devices";

const signUpStart = () => ({ type: "auth/SIGN_UP_START" });
const signUpSuccess = payload => ({ type: "auth/SIGN_UP_SUCCESS", payload });
const signUpFail = payload => ({ type: "auth/SIGN_UP_FAILURE", payload });

export const signUp = (name, email, password, navigate) => async dispatch => {
  dispatch(signUpStart());
  try {
    const body = {
      name,
      email,
      password
    };
    const response = await post("/signup", body);
    dispatch(signUpSuccess(response));
    navigate("SignUpConfirmation");
    dispatch(verifyEmail(email));

  } catch (error) {
    dispatch(signUpFail());
  }
};

const loginStart = () => ({ type: "auth/LOGIN_START" });
const loginSuccess = payload => ({ type: "auth/LOGIN_SUCCESS", payload });
const loginFail = payload => ({ type: "auth/LOGIN_FAILURE", payload });

export const login = (email, password, navigate) => async dispatch => {
  dispatch(loginStart());
  try {
    const body = {
      email,
      password
    };

    const response = await post("/login", body);
    setToken(response.jwt);
    dispatch(loginSuccess(response));
    navigate("Drawer");
    await(sendOneSignalTags(response.user));
    dispatch(createDevice(await getDeviceUserId(), await getDevicePushToken()));
  } catch (error) {
    dispatch(loginFail(error.message));
  }
};

const logoutStart = () => ({ type: "auth/LOGOUT_START" });
const logoutSuccess = () => ({ type: "auth/LOGOUT_SUCCESS" });
const logoutFail = () => ({ type: "auth/LOGOUT_FAILURE" });

export const logout = () => async dispatch => {
  dispatch(logoutStart());
  try {
    removeToken();
    dispatch(logoutSuccess());
  } catch (error) {
    dispatch(logoutFail(error.message));
  }
};

const linkedInLoginStart = () => ({ type: "auth/LINKEDIN_LOGIN_START" });
const linkedInLoginSuccess = payload => ({
  type: "auth/LINKEDIN_LOGIN_SUCCESS",
  payload
});
const linkedInLoginFail = () => ({ type: "auth/LINKEDIN_LOGIN_FAILURE" });

export const linkedInLogin = (token, onSuccess) => async dispatch => {
  dispatch(linkedInLoginStart());
  try {
    const response = await get(`/login/linkedin/token?access_token=${token.access_token}`);
    dispatch(linkedInLoginSuccess(response));
    onSuccess();

  } catch (error) {
    onSuccess();
    dispatch(linkedInLoginFail(error.message));
  }
};

const verifyEmailStart = () => ({ type: "auth/VERIFY_EMAIL_START" });
const verifyEmailSuccess = () => ({ type: "auth/VERIFY_EMAIL_SUCCESS" });
const verifyEmailFail = () => ({ type: "auth/VERIFY_EMAIL_FAILURE" });

export const verifyEmail = email => async dispatch => {
  dispatch(verifyEmailStart());
  try {
    const body = {
      email
    };

    const response = await post("/verify/", body);
    dispatch(verifyEmailSuccess(response));
  } catch (error) {
    dispatch(verifyEmailFail(error.message));
  }
};

const resetPasswordStart = () => ({ type: "auth/RESET_PASSWORD_START" });
const resetPasswordSuccess = () => ({ type: "auth/RESET_PASSWORD_SUCCESS" });
const resetPasswordFail = () => ({ type: "auth/RESET_PASSWORD_FAILURE" });

export const resetPassword = (email, onSuccess) => async dispatch => {
  dispatch(resetPasswordStart());
  try {
    const body = {
      email
    };
    const response = await post("/forgot/", body);
    dispatch(resetPasswordSuccess(response));
    onSuccess();
  } catch (error) {
    dispatch(resetPasswordFail(error.message));
  }
};
