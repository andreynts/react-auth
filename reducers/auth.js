const initialState = {
  jwt: "",
  isLoading: false,
  user: {
    id: null,
    email: "",
    username: null,
    name: "",
    createdAt: "",
    updatedAt: ""
  },
  token: "",
  error: ""
};

export default function(state: any = initialState, action: any) {
  switch (action.type) {
    case "auth/SIGN_UP_START":
      return { ...state, isLoading: true };
    case "auth/SIGN_UP_SUCCESS":
      return { ...state, isLoading: false, ...action.payload };
    case "auth/SIGN_UP_FAILURE":
      return { ...state, isLoading: false, error: action.payload };
    case "auth/LOGIN_START":
      return { ...state, isLoading: true };
    case "auth/LOGIN_SUCCESS":
      return { ...state, isLoading: false, ...action.payload };
    case "auth/LOGIN_FAILURE":
      return { ...state, isLoading: false, error: action.payload };
    case "auth/LOGOUT_START":
      return { ...state, isLoading: true };
    case "auth/LOGOUT_SUCCESS":
      return initialState;
    case "auth/LOGOUT_FAILURE":
      return { ...state, isLoading: false };
    case "auth/LINKEDIN_LOGIN_START":
      return { ...state, isLoading: true };
    case "auth/LINKEDIN_LOGIN_SUCCESS":
      return { ...state, isLoading: false, ...action.payload };
    case "auth/LINKEDIN_LOGIN_FAILURE":
      return { ...state, isLoading: false };

      case "auth/SET_USER":
        return {
            ...state,
            isLoading: false,
            ...action.payload
        };
    default:
      return state;
  }
}
